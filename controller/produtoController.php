<?php
include_once __DIR__ . "/../model/categoriaModel.php";

class ProdutoCtrl
{
    private $db;
    private $dash;
    /**
     * Construtor do Controller do Produto
     * Recebe os parâmetros do método POST para controlar o que fazer
     * @param dbConnector $db Objeto do Banco de Dados
     * @param bool $dash Booleana (True se Dashboard.php ativo, False o contrário)
     */
    public function __construct($db, $dash)
    {
        $this->db = $db;
        $this->dash = $dash;
        if(isset($_POST["action"]))
        {
            if($_POST["action"] == "Delete")
            {
                $this->deleteProd($_POST["id"]);
            }
            else if($_POST["action"] == "Edit")
            {
                $resultsprod = $this->getOneProd($_POST["id"]);
                $resultscat = $this->getNomeCat();
                include_once __DIR__ . "/../view/editProduct.php";
            }
            else if($_POST["action"] == "Save Changes")
            {
                $this->editProd($_POST["nameprod"], $_POST["skuprod"], $_POST["priceprod"], $_POST["qtdprod"], $_POST["fkprod"], $_POST["descprod"], $_POST["id"]);
            }
            else if($_POST["action"] == "Add Product")
            {
                $resultscat = $this->getNomeCat();
                include_once __DIR__ . "/../view/addProduct.php";
            }
            else if($_POST["action"] == "Submit Add")
            {
                $this->createProd($_POST["nameprod"], $_POST["skuprod"], $_POST["priceprod"], $_POST["qtdprod"], $_POST["fkprod"], $_POST["descprod"]);
            }
        }
        else
        {
            //$this->getNomeCat();
            $this->getProd();
        }
        
    }

    /**
     *  Funcao de Criar Produto
     * @param string $nomeprod Nome do Produto
     * @param string $skuprod SKU do Produto
     * @param string $precoprod Preço do Produto
     * @param array $fkprod ID das Categorias Selecionadas
     * @param string $descprod Descrição do Produto
     */
    public function createProd($nomeprod, $skuprod, $precoprod, $qtdprod, $fkprod, $descprod)
    {
        $modelprod = new ProdModel($this->db);
        $modelprod->setNomeProd($nomeprod);
        $modelprod->setSKUProd($skuprod);
        $modelprod->setPrecoProd($precoprod);
        $modelprod->setQtdProd($qtdprod);
        $modelprod->setFKProd($fkprod);
        $modelprod->setDescProd($descprod);
        $modelprod->addProduto();
        $this->getProd();
    }

    /**
     *  Função para deletar produto
     *  @param integer $id ID do produto a ser deletado
     */
    public function deleteProd($id)
    {
        $modelprod = new ProdModel($this->db);
        $modelprod->setIDProd($id);
        $modelprod->delProduto();
        $this->getProd();
    }

    /**
     *  Função para Retornar Produtos do banco de dados
     *  @return array $results Array com todas informações do Produto
     *  @return integer $rows Quantidade de Produtos Diferentes Criados no BD
     *  @return array $nomes Array com nomes das categorias dos produtos 
     */
    public function getProd()
    {
        $modelprod = new ProdModel($this->db);
        [$results, $rows] = $modelprod->getProdutos();
        
        if($this->dash != True)
        {
            $length = count($results);
            for($i = 0; $i < $length; $i++)
            {
                $modelprod->setSKUProd($results[$i]["sku"]);
                $idprod = $modelprod->getIDProduto();  
                $nomes[] = $modelprod->getCatPivo($idprod[0]['idproduto']);
            }
            $i = 0;
            include_once __DIR__ . "/../view/products.php";
        }
        else
        {
            return array($results, $rows);
        }
    }

    /**
     *  Funcao de Editar Produto
     * @param string $nomeprod Nome do Produto
     * @param string $skuprod SKU do Produto
     * @param string $precoprod Preço do Produto
     * @param array $fkprod ID das Categorias Selecionadas
     * @param string $descprod Descrição do Produto
     */
    public function editProd($nomeprod, $skuprod, $precoprod, $qtdprod, $fkprod, $descprod, $id)
    {
        $modelprod = new ProdModel($this->db);
        $modelprod->setNomeProd($nomeprod);
        $modelprod->setSKUProd($skuprod);
        $modelprod->setPrecoProd($precoprod);
        $modelprod->setQtdProd($qtdprod);
        $modelprod->setFKProd($fkprod);
        $modelprod->setDescProd($descprod);
        $modelprod->setIDProd($id);
        $modelprod->editProduto();
        $this->getProd();
    }

    /**
     *  Função que retorna um produto específico
     *  @param integer $id ID do produto selecionado
     *  @param array $results Array com informações do produto selecionado
     */
    public function getOneProd($id)
    {
        $modelprod = new ProdModel($this->db);
        $modelprod->setIDProd($id);
        $results = $modelprod->getOneProduto();
        return $results;
    }

    /**
     *  Função para retornar categorias da tabela categorias
     *  @return array Categorias do BD
     */
    public function getNomeCat()
    {
        include_once __DIR__ . "/../model/categoriaModel.php";
        $modelcat = new CategoriaModel($this->db);
        return $modelcat->getCategorias();
    }
    
}