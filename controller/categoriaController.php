<?php
include_once __DIR__ .  "/../model/produtoModel.php";

class CategoriaCtrl
{
    private $db;
    /**
     * Construtor do Controller da Categoria
     * Recebe os parâmetros do método POST para controlar o que fazer
     * @param dbConnector $db Objeto do Banco de Dados
     */
    public function __construct($db)
    {
        $this->db = $db;
        if(isset($_POST["action"]))
        {
            if($_POST["action"] == "Delete")
            {
                $this->deleteCategoria($_POST["id"]);
            }
            else if($_POST["action"] == "Edit")
            {
                $results = $this->getOneCat($_POST["id"]);
                include_once __DIR__ . "/../view/editCategory.php";
            }
            else if($_POST["action"] == "Save Changes")
            {
                $this->editCat($_POST["namecat"], $_POST["codcat"], $_POST["id"]);
            }
            else if($_POST["action"] == "Add Category")
            {
                $this->createCategory($_POST["namecat"], $_POST["codcat"]);
            }
        }
        else
        {
            $this->getCat();
        }
        
    }

    /**
     *  Função para Criar Categoria
     *  @param string $nomecat Nome da Categoria
     *  @param integer $codigocat Codigo da Categoria  
     */
    public function createCategory($nomecat, $codigocat)
    {
        $modelcat = new CategoriaModel($this->db);
        $modelcat->setNomeCat($nomecat);
        $modelcat->setCodCat($codigocat);
        $modelcat->addCategoria();
        $this->getCat();
    }

    /**
     *  Função para Deletar Categoria
     *  @param integer $id ID da categoria
     */
    public function deleteCategoria($id)
    {
        $modelcat = new CategoriaModel($this->db);
        $modelcat->setIDCat($id);
        $modelcat->delCategoria();
        $this->getCat();
        
    }

    /**
     *  Função para Retornar Todas Categorias do BD
     */
    public function getCat()
    {
        $modelcat = new CategoriaModel($this->db);
        $results = $modelcat->getCategorias();

        include_once __DIR__ . "/../view/categories.php";
    }

    /**
     *  Função para Editar Categoria
     *  @param string $nomecat Nome da Categoria
     *  @param integer $codigocat Codigo da Categoria 
     *  @param integer $id ID da Categoria a ser Editada
     */
    public function editCat($nomecat, $codigocat, $id)
    {
        $modelcat = new CategoriaModel($this->db);
        $modelcat->setNomeCat($nomecat);
        $modelcat->setCodCat($codigocat);
        $modelcat->setIDCat($id);
        $modelcat->editCategory();
        $this->getCat();
    }

    /**
     *  Função que Retorna apenas 1 Categoria
     *  @param integer $id ID da Categoria
     *  @return array $results Array com info da Categoria
     */
    public function getOneCat($id)
    {
        $modelcat = new CategoriaModel($this->db);
        $modelcat->setIDCat($id);
        $results = $modelcat->getOneCategoria();
        return $results;
    }
    
}