-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema crud_desafio
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema crud_desafio
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `crud_desafio` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `crud_desafio` ;

-- -----------------------------------------------------
-- Table `crud_desafio`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `crud_desafio`.`categoria` (
  `idcategoria` INT(11) NOT NULL AUTO_INCREMENT,
  `nomecategoria` VARCHAR(45) NULL DEFAULT NULL,
  `codigocategoria` INT(5) NULL DEFAULT NULL,
  PRIMARY KEY (`idcategoria`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `crud_desafio`.`produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `crud_desafio`.`produto` (
  `idproduto` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeproduto` VARCHAR(45) NULL DEFAULT NULL,
  `sku` VARCHAR(45) NULL DEFAULT NULL,
  `descricaoproduto` VARCHAR(255) NULL DEFAULT NULL,
  `quantidadeproduto` INT(11) NULL DEFAULT NULL,
  `preco` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idproduto`))
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `crud_desafio`.`produto_categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `crud_desafio`.`produto_categoria` (
  `idpivo` INT(11) NOT NULL AUTO_INCREMENT,
  `idproduto` INT(11) NOT NULL,
  `idcategoria` INT(11) NOT NULL,
  PRIMARY KEY (`idpivo`, `idproduto`, `idcategoria`),
  INDEX `fk_produto_has_categoria_categoria1_idx` (`idcategoria` ASC) ,
  INDEX `fk_produto_has_categoria_produto_idx` (`idproduto` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 14
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
