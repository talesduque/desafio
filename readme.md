# Como Rodar o Projeto

1. Clonar este repositório.
2. Execute o script ```desafio.sql``` para criar o database.
3. Crie um host para acessar a pasta raiz do repositório.
4. Insira informações de conexão do banco de dados, como host, porta, user e senha no arquivo ```dbCONFIG.php```.
5. Acesse utilizando o browser

## Requisitos

* MySQL 8.0
* PHP 7.x

## Estrutura do Banco de Dados

A estrutura para atingir o relacionamento n:n utilizada foi a descrita abaixo. Utilizou-se uma tabela pivô que relaciona vários produtos à várias categorias e vice-versa.

![Tabelas](/RelacaoTabelas.png)

## Autor

Tales Faceroli Duque

E-mail: talesfduque@gmail.com 

[LinkedIn](https://www.linkedin.com/in/talesfaceroliduque/)