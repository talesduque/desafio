
<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Edit Product</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="view/css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script></head>
  <!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="view/images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="view/dashboard.php"><img src="view/images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="ctrlBS.php" class="link-menu">Categories</a></li>
      <li><a href="ctrlBSprod.php" class="link-menu">Products</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="view/dashboard.php" class="link-logo"><img src="view/images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header -->
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Edit Product</h1>
    
    <form method="POST" class="actions" action="ctrlBSprod.php">
      <div class="input-field">
        <label for="product-sku" class="label">New SKU</label>
        <input type="text" name="skuprod" id="prod-sku" class="input-text" value="<?php echo $resultsprod[0]['sku']?>"/>
        
      </div>

      <div class="input-field">
        <label for="product-name" class="label">New Product Name</label>
        <input type="text" name="nameprod" id="prod-name" class="input-text" value="<?php echo $resultsprod[0]['nomeproduto']?>"/>
        
      </div>

      <div class="input-field">
        <label for="product-price" class="label">New Product Price</label>
        <input type="text" name="priceprod" id="prod-price" class="input-text" value="<?php echo $resultsprod[0]['preco']?>"/>
        
      </div>

      <div class="input-field">
        <label for="product-quant" class="label">New Product Quantity</label>
        <input type="text" name="qtdprod" id="prod-quant" class="input-text" value="<?php echo $resultsprod[0]['quantidadeproduto']?>"/>
        
      </div>

      <div class="input-field">
        <label for="category" class="label">New Category</label>
        <select multiple id="category" class="input-text" name="fkprod[]">
          <?php
            
            include_once __DIR__ . "/../controller/produtoController.php";
            include_once __DIR__ . "/../db.php";
            $db = new dbConnector();
            $prodctrl = new ProdutoCtrl($db, false);
            $resultscat = $prodctrl->getNomeCat();
            foreach($resultscat as $categorias):
          ?>
        
          <option value="<?php echo $categorias['idcategoria'];?>"><?php echo $categorias['nomecategoria'];?></option>
          <?php endforeach;?>
        </select>
      </div>

      <div class="input-field">
        <label for="description" class="label">New Description</label>
        <textarea id="description" name="descprod" class="input-text"><?php echo $resultsprod[0]['descricaoproduto']?></textarea>
      </div>

      <div class="actions-back">
        <a href="ctrlBSprod.php" class="action back">Back</a>
        <input type="hidden" name="id" value="<?php echo $resultsprod[0]['idproduto']?>"/> 
        <input class="btn-submit btn-action" name="action" type="submit" value="Save Changes"/>
      </div>
    </form>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="footer-image">
	  <img src="view/images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
	</div>
	<div class="email-content">
	  <span>go@jumpers.com.br</span>
	</div>
</footer>
 <!-- Footer --></body>
</html>
