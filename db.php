<?php
include_once __DIR__ . "/dbCONFIG.php"; 
class dbConnector
{
    private $host;
    private $user;
    private $pwd;
    private $dbName;
    private $port;
    public function __construct()
    {
        $this->host = $GLOBALS['dbconfigs']['host'];
        $this->user = $GLOBALS['dbconfigs']['user'];
        $this->pwd = $GLOBALS['dbconfigs']['passwd'];
        $this->dbName = $GLOBALS['dbconfigs']['dbname'];
        $this->port = $GLOBALS['dbconfigs']['port'];
        $this->connect();
    }
    
    public function connect()
    {
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbName . ';port=' . $this->port;
        $pdo = new PDO ($dsn, $this->user, $this->pwd);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    }
    
}
