<?php

class CategoriaModel
{
    private $db;
    private $nomecat;
    private $codcat; 
    private $id;   

    /**
     *  Construtor da Classe Categoria Model
     *  @param dbConnector $db  Objeto do Banco de Dados
     */
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Setar o valor do atributo $nomecat
     * @param string $nomecategoria Nome da Categoria
     */
    public function setNomeCat($nomecategoria)
    {
        $this->nomecat = $nomecategoria;
    }

    /**
     * Setar o valor do atributo $id
     * @param integer $idcategoria ID da Categoria
     */
    public function setIDCat($idcategoria)
    {
        $this->id = $idcategoria;
    }

    /**
     * Setar o valor do atributo $codcat
     * @param integer $codcategoria Codigo da Categoria
     */
    public function setCodCat($codcategoria)
    {
        $this->codcat = $codcategoria;
    }
    
    
    /**
     *  Retorna Categorias do Banco
     *  @return array $results Resultados do Fetch no Banco
     */
    public function getCategorias()
    {
        $sql = "SELECT * FROM categoria";
        $stmt = $this->db->connect()->query($sql);

        $results = $stmt->fetchAll();
        return $results;
    }

    /**
     *  Retorna Uma Categoria do Banco
     *  @param integer $id da categoria a editar
     *  @return array $results Resultados do Fetch no Banco
     */
    public function getOneCategoria()
    {
        $sql = "SELECT * FROM categoria WHERE idcategoria = ?";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$this->id]);
        $results = $stmt->fetchAll();
        return $results;
    }

    /**
     *  Adicionar Categoria no Banco de Dados
     */
    public function addCategoria()
    {
        $sql = "INSERT INTO categoria(nomecategoria, codigocategoria) VALUES (?,?)";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$this->nomecat, $this->codcat]);
    }

    /**
     * Deleta Categoria no Banco de Dados
     */ 
    public function delCategoria()
    {
        $sql = "DELETE FROM categoria WHERE idcategoria = ?";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$this->id]);
    }

    /**
     *  Editar Categoria no Banco de Dados
     */
    public function editCategory()
    {
        $sql = "UPDATE categoria SET nomecategoria = ?, codigocategoria = ? WHERE idcategoria = ?";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$this->nomecat, $this->codcat, $this->id]);
        $sql = "UPDATE produto SET categoria = ? WHERE fk_idcat = ?";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$this->nomecat, $this->id]);
    }
} 

?>