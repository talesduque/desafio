<?php

class ProdModel
{
    private $db;
    private $nomeprod;
    private $skuprod;
    private $precoprod;
    private $qtdprod; 
    private $descprod;
    private $fkprod = array();
    private $id;   

    /**
     *  Construtor da Classe ProdModel
     *  @param dbConnector $db  Objeto do Banco de Dados
     */
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Setar o valor do atributo $nomecat
     * @param string $nomeprod Nome do Produto
     */
    public function setNomeProd($nomeprod)
    {
        $this->nomeprod = strtoupper($nomeprod);
    }

    /**
     * Setar o valor do atributo $id
     * @param integer $idprod ID do Produto
     */
    public function setIDProd($idprod)
    {
        $this->id = $idprod;
    }

    /**
     * Setar o valor do atributo $precoprod
     * @param string $precoprod Preço do Produto
     */
    public function setPrecoProd($precoprod)
    {
        $this->precoprod = "R$".$precoprod;
    }

    /**
     * Setar o valor do atributo $skuprod
     * @param integer $skuprod SKU do Produto
     */
    public function setSKUProd($skuprod)
    {

        $this->skuprod = $skuprod;
    }

    /**
     * Setar o valor do atributo $qtdprod
     * @param integer $qtdprod Quantidade do Produto
     */
    public function setQtdProd($qtdprod)
    {
        $this->qtdprod = $qtdprod;
    }


    /**
     * Setar o valor do atributo $fkprod
     * @param integer $fkprod Foreign Key do Produto Para Categoria
     */
    public function setFKProd($fkprod)
    {
        $this->fkprod = $fkprod;
    }

    /**
     * Setar o valor do atributo $descprod
     * @param string $descprod Descrição do Produto
     */
    public function setDescProd($descprod)
    {
        $this->descprod = $descprod;
    }
    
    /**
     *  Retorna Produtos do Banco
     *  @return array $results Resultados do Fetch no Banco
     */
    public function getProdutos()
    {
        $sql = "SELECT * FROM produto";
        $stmt = $this->db->connect()->query($sql);
        $rows = $stmt->rowCount();
        $results = $stmt->fetchAll();
        return array ($results, $rows);
    }

    /**
     *  Retorna Um Produto do Banco
     *  @return array $results Resultados do Fetch no Banco
     */
    public function getOneProduto()
    {
        $sql = "SELECT * FROM produto WHERE idproduto = ?";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$this->id]);
        $results = $stmt->fetchAll();
        return $results;
    }

    /**
     *  Retorna o ID do Produto Selecionado
     *  @return array $results Resultados do Fetch no Banco
     */
    public function getIDProduto()
    {
        $sql = "SELECT idproduto FROM produto WHERE sku = ?";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$this->skuprod]);
        $results = $stmt->fetchAll();
        return $results;
    }

    /**
     *  Adicionar Produto no Banco de Dados
     */
    public function addProduto()
    {
        $sql = "INSERT INTO produto(nomeproduto, sku, descricaoproduto, quantidadeproduto, preco) VALUES (?,?,?,?,?)";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$this->nomeprod, $this->skuprod, $this->descprod, $this->qtdprod, $this->precoprod]);
        $id = $this->getIDProduto();
        $sql = "INSERT INTO produto_categoria (idproduto, idcategoria) VALUES (?,?)";
        $stmt = $this->db->connect()->prepare($sql);
        $length = count($this->fkprod);
        for($i = 0; $i < $length; $i++)
        {
            $stmt->execute([intval($id[0]['idproduto']), intval($this->fkprod[$i])]);
        }
    }

    /**
     * Deleta Produto no Banco de Dados
     */ 
    public function delProduto()
    {
        $sql = "DELETE FROM produto WHERE idproduto = ?";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$this->id]);
    }

    /**
     *  Editar Produto no Banco de Dados
     */
    public function editProduto()
    {
        $sql = "UPDATE produto SET nomeproduto = ?, sku = ?, descricaoproduto = ?, quantidadeproduto = ?, preco = ?  WHERE idproduto = ?";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$this->nomeprod, $this->skuprod, $this->descprod, $this->qtdprod, $this->precoprod, $this->id]);
        $id = $this->getIDProduto();
        $sql = "DELETE FROM produto_categoria WHERE idproduto = ?";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([intval($id[0]['idproduto'])]);
        $sql = "INSERT INTO produto_categoria (idproduto, idcategoria) VALUES (?,?)";
        $stmt = $this->db->connect()->prepare($sql);
        if(is_array($this->fkprod))
        {
            $length = count($this->fkprod);
            for($i = 0; $i < $length; $i++)
            {
                $stmt->execute([intval($id[0]['idproduto']), intval($this->fkprod[$i])]);
            }
        }
        else
        {
            $stmt->execute([intval($id[0]['idproduto']), intval($this->fkprod)]);
        }
        

    }

    /**
     *  Funcao para coletar nome das categorias do produto a partir da tabela pivo
     *  @param integer $id ID do Produto
     *  @return array $namefinal Nomes Das Categorias do Produto
     */

    public function getCatPivo($id)
    {
        $sql = "SELECT idcategoria FROM produto_categoria WHERE idproduto = ?";
        $stmt = $this->db->connect()->prepare($sql);
        $stmt->execute([$id]);
        $idcat = $stmt->fetchAll();
        $length = count($idcat);
        for($i = 0; $i < $length; $i++)
        {
            $sql = "SELECT nomecategoria FROM categoria WHERE idcategoria = ?";
            $stmt = $this->db->connect()->prepare($sql);
            $stmt->execute([$idcat[$i]['idcategoria']]);
            $name = $stmt->fetchAll();
            $namefinal[$i] = $name[0]['nomecategoria']; 
        }
        return $namefinal;
    }

} 

?>